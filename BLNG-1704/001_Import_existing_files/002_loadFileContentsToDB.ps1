$MyServer = "localhost";
$MyPort = 5432;
$MyDB = "BizAnalyticsPAT_Import";
$MyUid ="postgres"
$MyPass ="postgres"

$targDir = "C:\Git\biz-analytics-pat\environments\production\pricings\";

$query =  @"
TRUNCATE TABLE public.billableitemssql;
TRUNCATE TABLE public.config;
TRUNCATE TABLE public.fileimport;
"@;
$conn = New-Object System.Data.Odbc.OdbcConnection
$conn.ConnectionString= "Driver={PostgreSQL UNICODE(x64)};Server=$MyServer;Port=$MyPort;Database=$MyDB;Uid=$MyUid;Pwd=$MyPass;"
$cmd = new-object System.Data.Odbc.OdbcCommand($query,$conn)
$conn.open()
$cmd.ExecuteNonQuery()
$conn.close()

$childFiles = $(Get-ChildItem -Path $targDir -Recurse -File );

foreach($file in $childFiles) 
{
    $fileName = $file.Name  #file name
    $filePath = $file.FullName  #entire file path
    $parentPath = ($file.Directory).FullName    #parent folder path
    $fileContents = Get-Content $filePath

    $filePath  # print it so we know where we are

    if ($fileContents -ne $null)
    {
        $parentPath = $parentPath.Replace("'", "''");
        $fileName = $fileName.Replace("'", "''");
        $fileContents = $fileContents.Replace("'", "''");

        $query =  "INSERT INTO public.fileImport(folderStructure, fileName, fileContents) VALUES ('$parentPath', '$fileName', '$fileContents')";
        $cmd = new-object System.Data.Odbc.OdbcCommand($query,$conn)
        $conn.open()
        $cmd.ExecuteNonQuery()
        $conn.close()
    }
}