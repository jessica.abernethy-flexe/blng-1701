SELECT 
	COUNT(1) AS num
	--, filecontents5subquery1
	--, filecontents5subquery2
	, filecontents5_insert
	, filecontents5_select
	, filecontents5_from
	, filecontents5_where
	, filecontents5_groupby
	, filecontents5_having
	, filecontents5_orderby
	, filecontents5_limit
	, filecontents5_comment
	, filecontents5
FROM public.billableitemssql
GROUP BY
	--filecontents5subquery1
	--, filecontents5subquery2
	--, 
	filecontents5_insert
	, filecontents5_select
	, filecontents5_from
	, filecontents5_where
	, filecontents5_groupby
	, filecontents5_having
	, filecontents5_orderby
	, filecontents5_limit
	, filecontents5_comment
	, filecontents5
ORDER BY
	--filecontents5subquery1
	--, filecontents5subquery2
	--, 
	filecontents5_from
	, filecontents5_where
	, filecontents5_insert
	, filecontents5_select
	, filecontents5_groupby
	, filecontents5_having
	, filecontents5_orderby
	, filecontents5_limit
	, filecontents5_comment
	, filecontents5;

/*
SELECT COUNT(1) AS num, filecontents5_insert 	FROM public.billableitemssql GROUP BY filecontents5_insert 		ORDER BY 2
SELECT COUNT(1) AS num, filecontents5_select 	FROM public.billableitemssql GROUP BY filecontents5_select 		ORDER BY 2
SELECT COUNT(1) AS num, filecontents5_from 		FROM public.billableitemssql GROUP BY filecontents5_from 		ORDER BY 2
SELECT COUNT(1) AS num, filecontents5_where 	FROM public.billableitemssql GROUP BY filecontents5_where 		ORDER BY 2
SELECT COUNT(1) AS num, TRIM(filecontents5_groupby) 	FROM public.billableitemssql GROUP BY TRIM(filecontents5_groupby) 	ORDER BY 2
SELECT COUNT(1) AS num, filecontents5_having 	FROM public.billableitemssql GROUP BY filecontents5_having 		ORDER BY 2
SELECT COUNT(1) AS num, filecontents5_orderby 	FROM public.billableitemssql GROUP BY filecontents5_orderby 	ORDER BY 2
SELECT COUNT(1) AS num, filecontents5_limit 	FROM public.billableitemssql GROUP BY filecontents5_limit 		ORDER BY 2
SELECT COUNT(1) AS num, filecontents5_comment 	FROM public.billableitemssql GROUP BY filecontents5_comment 	ORDER BY 2
SELECT COUNT(1) AS num, filecontents5subquery1 	FROM public.billableitemssql GROUP BY filecontents5subquery1 	ORDER BY 2
SELECT COUNT(1) AS num, filecontents5subquery2 	FROM public.billableitemssql GROUP BY filecontents5subquery2 	ORDER BY 2
SELECT COUNT(1) AS num, filecontents5 			FROM public.billableitemssql GROUP BY filecontents5 			ORDER BY 2
SELECT COUNT(1) AS num, filecontents4 			FROM public.billableitemssql GROUP BY filecontents4 			ORDER BY 2
*/