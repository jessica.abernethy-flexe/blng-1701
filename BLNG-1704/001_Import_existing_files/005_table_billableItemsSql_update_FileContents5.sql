UPDATE public.billableitemssql
SET filecontents5 				= null
	, fileContents5subquery1 	= null
	, fileContents5subquery2	= null
	, filecontents5_insert 		= null
	, filecontents5_select 		= null
	, filecontents5_from 		= null
	, filecontents5_where 		= null
	, filecontents5_groupby		= null
	, filecontents5_having 		= null
	, filecontents5_orderby		= null
	, filecontents5_limit 		= null
	, filecontents5_comment		= null;

WITH 
	List0 AS (
		SELECT
			fileImportId
			, REPLACE(fileContents4, '( -- Date of HWM SELECT', '( SELECT') AS fileContents4
		FROM public.billableitemssql
	)		
	, List1 AS (
		SELECT
			fileImportId
			, fileContents4
			, (LENGTH(fileContents4) - LENGTH(REPLACE(fileContents4, '( SELECT '	, ''))) / LENGTH('( SELECT ') AS numSubQueries
		FROM List0
		WHERE strpos(fileContents4, '( SELECT ') > 0
	)
	, List2 AS (
		SELECT
			*
			, strpos(filecontents4, '( SELECT ') + 1 AS startSubQuery1
			, LENGTH(filecontents4) - strpos(REVERSE(filecontents4), ' )') AS endSubQuery1
		FROM List1
	)
	, List3 AS (
		SELECT 
			*
			, SUBSTRING(filecontents4, startSubQuery1, endSubQuery1 - startSubQuery1) AS subQuery1
		FROM List2
	)
	, List4 AS (
		SELECT 
			*
			, REPLACE(filecontents4, subQuery1, 'subQuery1') AS filecontents5
		FROM List3
	)
	, List5 AS (
		SELECT
			fileImportId
			, subQuery1
		FROM List4
		WHERE numSubQueries > 1
	)
	, List6 AS (
		SELECT
			*
			, strpos(subQuery1, '( SELECT ') + 1 AS startSubQuery2
			, LENGTH(subQuery1) - strpos(REVERSE(subQuery1), ' )') AS endSubQuery2
		FROM List5
	)
	, List7 AS (
		SELECT 
			*
			, SUBSTRING(subQuery1, startSubQuery2, endSubQuery2 - startSubQuery2) AS subQuery2
		FROM List6
	)
UPDATE public.billableitemssql AS BI
SET filecontents5 				= A.filecontents5
	, fileContents5subquery1 	= CASE
									WHEN B.fileImportId IS NULL THEN A.subQuery1
									ELSE REPLACE(B.subQuery1, B.subQuery2, 'subQuery2')
								END
	, fileContents5subquery2 	= B.subQuery2
FROM List4 AS A
LEFT JOIN List7 AS B
	ON A.fileImportId = B.fileImportId
WHERE BI.fileImportId = A.fileImportId;

UPDATE public.billableitemssql
SET filecontents5 = filecontents4
WHERE filecontents5 IS NULL;

--SELECT * FROM public.billableitemssql;


UPDATE public.billableitemssql
SET filecontents5_insert 	= null
	, filecontents5_select 	= null
	, filecontents5_from 	= null
	, filecontents5_where 	= null
	, filecontents5_groupby = null
	, filecontents5_having 	= null
	, filecontents5_orderby = null
	, filecontents5_limit 	= null
	, filecontents5_comment	= null;

DROP TABLE IF EXISTS temp_split;

CREATE TEMPORARY TABLE temp_split (
	fileImportId 				integer 			NOT NULL
	, fileContents5				character varying	NOT NULL
	, charStartSelect			integer				NULL
	, charStartFrom				integer				NULL
	, charStartWhere			integer				NULL
	, charStartGroupBy			integer				NULL
	, charStartHaving			integer				NULL
	, charStartOrderBy			integer				NULL
	, charStartLimit			integer				NULL
	, charStartComment			integer				NULL
	, numSelects				integer				NULL
	, numFroms					integer				NULL
	, numWheres					integer				NULL
	, numGroupBys				integer				NULL
	, numHavings				integer				NULL
	, numOrderBys				integer				NULL
	, numLimits					integer				NULL
	, numComments				integer				NULL
	, fileContents5_Insert		character varying	NULL 		COLLATE pg_catalog."default"
	, fileContents5_Select		character varying	NULL 		COLLATE pg_catalog."default"
	, fileContents5_From		character varying	NULL 		COLLATE pg_catalog."default"
	, fileContents5_SubQuery	character varying	NULL 		COLLATE pg_catalog."default"
	, fileContents5_Where		character varying	NULL 		COLLATE pg_catalog."default"
	, fileContents5_GroupBy		character varying	NULL 		COLLATE pg_catalog."default"
	, fileContents5_Having		character varying	NULL 		COLLATE pg_catalog."default" 
	, fileContents5_OrderBy		character varying	NULL 		COLLATE pg_catalog."default" 
	, fileContents5_Limit		character varying	NULL 		COLLATE pg_catalog."default"
	, fileContents5_Comment		character varying	NULL 		COLLATE pg_catalog."default"
	);

INSERT INTO temp_split (
	fileImportId
	, fileContents5
	, numSelects
	, numFroms
	, numWheres
	, numGroupBys
	, numOrderBys
	, numHavings
	, numLimits
	, numComments
	)
SELECT
	fileImportId
	, fileContents5
	, (LENGTH(fileContents5) - LENGTH(REPLACE(fileContents5, 'SELECT '		, ''))) / LENGTH('SELECT ')
	, (LENGTH(fileContents5) - LENGTH(REPLACE(fileContents5, 'FROM '		, ''))) / LENGTH('FROM ')
	, (LENGTH(fileContents5) - LENGTH(REPLACE(fileContents5, 'WHERE '		, ''))) / LENGTH('WHERE ')
	, (LENGTH(fileContents5) - LENGTH(REPLACE(fileContents5, 'GROUP BY '	, ''))) / LENGTH('GROUP BY ')
	, (LENGTH(fileContents5) - LENGTH(REPLACE(fileContents5, 'ORDER BY '	, ''))) / LENGTH('ORDER BY ')
	, (LENGTH(fileContents5) - LENGTH(REPLACE(fileContents5, 'HAVING '		, ''))) / LENGTH('HAVING ')
	, (LENGTH(fileContents5) - LENGTH(REPLACE(fileContents5, 'LIMIT '		, ''))) / LENGTH('LIMIT ')
	, (LENGTH(fileContents5) - LENGTH(REPLACE(fileContents5, '--'			, ''))) / LENGTH('--')
FROM public.billableitemssql;

UPDATE temp_split SET charStartSelect 	= strpos(fileContents5, 'SELECT '	);
UPDATE temp_split SET charStartFrom 	= strpos(fileContents5, 'FROM '		) 	WHERE numFroms 		= 1;
UPDATE temp_split SET charStartWhere 	= strpos(fileContents5, 'WHERE '	) 	WHERE numWheres 	= 1;
UPDATE temp_split SET charStartGroupBy 	= strpos(fileContents5, 'GROUP BY '	) 	WHERE numGroupBys 	= 1;
UPDATE temp_split SET charStartOrderBy 	= strpos(fileContents5, 'ORDER BY '	) 	WHERE numOrderBys 	= 1;
UPDATE temp_split SET charStartHaving 	= strpos(fileContents5, 'HAVING '	) 	WHERE numHavings 	= 1;
UPDATE temp_split SET charStartLimit 	= strpos(fileContents5, 'LIMIT '	) 	WHERE numLimits 	= 1;
UPDATE temp_split SET charStartComment 	= strpos(fileContents5, '--'		)	WHERE numComments 	= 1;

UPDATE temp_split
SET fileContents5_insert 	= LEFT(fileContents5, charstartselect - 1)
	, fileContents5_select 	= SUBSTRING(fileContents5, charstartselect, 
										CASE
											WHEN charStartFrom 		IS NOT NULL THEN charStartFrom
											WHEN charStartWhere 	IS NOT NULL THEN charStartWhere
											WHEN charStartGroupBy 	IS NOT NULL THEN charStartGroupBy
											WHEN charStartHaving > charStartOrderBy THEN charStartOrderBy
											WHEN charStartOrderBy 	IS NOT NULL THEN charStartOrderBy
											WHEN charStartHaving 	IS NOT NULL THEN charStartHaving
											WHEN charStartLimit 	IS NOT NULL THEN charStartLimit
											WHEN charStartComment 	IS NOT NULL THEN charStartComment
											ELSE LENGTH(fileContents5) + 1
										END - charstartselect);
										
UPDATE temp_split
SET fileContents5_from 	= SUBSTRING(fileContents5, charstartfrom, 
										CASE
											WHEN charStartWhere 	IS NOT NULL THEN charStartWhere
											WHEN charStartGroupBy 	IS NOT NULL THEN charStartGroupBy
											WHEN charStartHaving > charStartOrderBy THEN charStartOrderBy
											WHEN charStartOrderBy 	IS NOT NULL THEN charStartOrderBy
											WHEN charStartHaving 	IS NOT NULL THEN charStartHaving
											WHEN charStartLimit 	IS NOT NULL THEN charStartLimit
											WHEN charStartComment 	IS NOT NULL THEN charStartComment
											ELSE LENGTH(fileContents5) + 1
										END - charstartfrom)
WHERE charstartfrom IS NOT NULL;

UPDATE temp_split
SET fileContents5_where 	= SUBSTRING(fileContents5, charStartWhere, 
										CASE
											WHEN charStartGroupBy 	IS NOT NULL THEN charStartGroupBy
											WHEN charStartHaving > charStartOrderBy THEN charStartOrderBy
											WHEN charStartOrderBy 	IS NOT NULL THEN charStartOrderBy
											WHEN charStartHaving 	IS NOT NULL THEN charStartHaving
											WHEN charStartLimit 	IS NOT NULL THEN charStartLimit
											WHEN charStartComment 	IS NOT NULL THEN charStartComment
											ELSE LENGTH(fileContents5) + 1
										END - charStartWhere)
WHERE charStartWhere IS NOT NULL;

UPDATE temp_split
SET fileContents5_GroupBy 	= SUBSTRING(fileContents5, charStartGroupBy, 
										CASE
											WHEN charStartHaving > charStartOrderBy THEN charStartOrderBy
											WHEN charStartHaving 	IS NOT NULL THEN charStartHaving - 1
											WHEN charStartOrderBy 	IS NOT NULL THEN charStartOrderBy
											WHEN charStartLimit 	IS NOT NULL THEN charStartLimit
											WHEN charStartComment 	IS NOT NULL THEN charStartComment
											ELSE LENGTH(fileContents5)
										END - charStartGroupBy + 1)
WHERE charStartGroupBy IS NOT NULL;

UPDATE temp_split
SET fileContents5_Having 	= SUBSTRING(fileContents5, charStartHaving, 
										CASE
											WHEN charStartHaving < charStartOrderBy THEN charStartOrderBy
											WHEN charStartLimit 	IS NOT NULL THEN charStartLimit
											WHEN charStartComment 	IS NOT NULL THEN charStartComment - 1
											ELSE LENGTH(fileContents5)
										END - charStartHaving + 1)
WHERE charStartHaving IS NOT NULL;

UPDATE temp_split
SET fileContents5_OrderBy 	= SUBSTRING(fileContents5, charStartOrderBy, 
										CASE
											WHEN charStartHaving > charStartOrderBy THEN charStartHaving
											WHEN charStartLimit 	IS NOT NULL THEN charStartLimit
											WHEN charStartComment 	IS NOT NULL THEN charStartComment
											ELSE LENGTH(fileContents5) + 1
										END - charStartOrderBy)
WHERE charStartOrderBy IS NOT NULL;

UPDATE temp_split
SET fileContents5_limit 	= CASE
								WHEN charStartComment > charStartLimit THEN SUBSTRING(fileContents5, charstartlimit, charStartComment - charStartLimit + 1)
								ELSE RIGHT(fileContents5, LENGTH(fileContents5) - charStartLimit + 1)
						   END
WHERE charStartLimit IS NOT NULL;

UPDATE temp_split
SET fileContents5_comment 	= RIGHT(fileContents5, LENGTH(fileContents5) - charStartComment + 1)
WHERE charStartComment IS NOT NULL
	AND (charStartComment > charStartLimit
		 OR (charStartLimit IS NULL
			 	AND charStartComment > charStartWhere
			 )
		 );

UPDATE public.billableitemssql AS B
SET fileContents5_insert 		= T.fileContents5_insert
	, fileContents5_select 		= T.fileContents5_select
	, fileContents5_from 		= T.fileContents5_from
	, fileContents5_where 		= T.fileContents5_where
	, fileContents5_groupby 	= T.fileContents5_groupby
	, fileContents5_having 		= T.fileContents5_having
	, fileContents5_orderby 	= T.fileContents5_orderby
	, fileContents5_limit 		= T.fileContents5_limit
	, fileContents5_comment 	= T.fileContents5_comment
FROM temp_split AS T
WHERE T.fileImportId = B.fileImportId;

SELECT * 
FROM public.billableitemssql
ORDER BY
	CASE WHEN fileContents5_insert IS NULL THEN 2 ELSE 1 END
	, CASE WHEN fileContents5_select IS NULL THEN 2 ELSE 1 END
	, CASE WHEN fileContents5_from IS NULL THEN 2 ELSE 1 END
	, CASE WHEN fileContents5_where IS NULL THEN 2 ELSE 1 END
	, CASE WHEN fileContents5_groupby IS NULL THEN 2 ELSE 1 END
	, CASE WHEN fileContents5_having IS NULL THEN 2 ELSE 1 END
	, CASE WHEN fileContents5_orderby IS NULL THEN 2 ELSE 1 END
	, CASE WHEN fileContents5_limit IS NULL THEN 2 ELSE 1 END
	, CASE WHEN fileContents5_comment IS NULL THEN 2 ELSE 1 END;