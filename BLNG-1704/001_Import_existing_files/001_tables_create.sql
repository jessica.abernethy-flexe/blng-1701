-- Table: public.fileimport

DROP TABLE IF EXISTS public.fileimport;

CREATE TABLE IF NOT EXISTS public.fileimport
(
    fileImportId 		integer 					NOT NULL 	GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    folderStructure 	character varying 			NOT NULL	COLLATE pg_catalog."default",
    fileName 			character varying 			NOT NULL	COLLATE pg_catalog."default",
    fileContents 		character varying 			NOT NULL	COLLATE pg_catalog."default",
    Timestamp 			timestamp with time zone 	NOT NULL 	DEFAULT CURRENT_TIMESTAMP,
    parentFolderName1	character varying 			NULL		COLLATE pg_catalog."default",
    parentFolderName2	character varying 			NULL		COLLATE pg_catalog."default",
    parentFolderName3	character varying 			NULL		COLLATE pg_catalog."default",
    CONSTRAINT fileimport_pkey PRIMARY KEY (fileImportId)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.fileimport
    OWNER to postgres;

-- Table: public.config

DROP TABLE IF EXISTS public.config;

CREATE TABLE IF NOT EXISTS public.config
(
    fileImportId 	integer 			NOT NULL,
    uid 			character varying 	NOT NULL	COLLATE pg_catalog."default",
    label 			character varying 	NULL		COLLATE pg_catalog."default",
    feeDescription 	character varying 	NULL		COLLATE pg_catalog."default",
    reservationId 	integer				NULL,
    feeCategory 	character varying 	NULL		COLLATE pg_catalog."default",
    unitOfMeasure 	character varying 	NULL		COLLATE pg_catalog."default",
    shipperRate 	numeric				NULL,
    warehouseRate 	numeric				NULL,
    CONSTRAINT config_pkey PRIMARY KEY (fileImportId)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.config
    OWNER to postgres;

-- Table: public.billableItemsSql

DROP TABLE IF EXISTS public.billableItemsSql;

CREATE TABLE IF NOT EXISTS public.billableItemsSql
(
    fileImportId 			integer 			NOT NULL,
    fileContents 			character varying	NOT NULL 	COLLATE pg_catalog."default",
    fileContents2 			character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents3 			character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents4 			character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5 			character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5subquery1	character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5subquery2	character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_Insert	character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_Select	character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_From		character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_Where		character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_GroupBy	character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_Having	character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_OrderBy	character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_Limit		character varying	NULL 		COLLATE pg_catalog."default", 
    fileContents5_Comment	character varying	NULL 		COLLATE pg_catalog."default", 
    timeZone 				character varying	NULL 		COLLATE pg_catalog."default", 
    interval_StartDate 		character varying	NULL 		COLLATE pg_catalog."default", 
    interval_EndDate 		character varying	NULL 		COLLATE pg_catalog."default", 
    deliveryType 			character varying	NULL 		COLLATE pg_catalog."default", 
    inboundType 			character varying	NULL 		COLLATE pg_catalog."default", 
    shipmentType 			character varying	NULL 		COLLATE pg_catalog."default", 
    unitOfMeasure 			character varying	NULL 		COLLATE pg_catalog."default", 
    inventoryGroupId		character varying	NULL 		COLLATE pg_catalog."default", 
    inventoryGroupName		character varying	NULL 		COLLATE pg_catalog."default", 
    inventoryGroupIds		character varying	NULL 		COLLATE pg_catalog."default", 
    inventoryGroupNames		character varying	NULL 		COLLATE pg_catalog."default", 
    itemId					character varying	NULL 		COLLATE pg_catalog."default", 
    itemIds					character varying	NULL 		COLLATE pg_catalog."default", 
	-- completed_at_DOW
	-- lever_start_date
	-- lever_end_date
	
	
    CONSTRAINT billableItemsSql_pkey PRIMARY KEY (fileImportId)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.billableItemsSql
    OWNER to postgres;
