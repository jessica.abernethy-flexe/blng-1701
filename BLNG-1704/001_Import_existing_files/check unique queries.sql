SELECT COUNT(1), REPLACE(REPLACE(REPLACE(filecontents4, ' )', ')'), '( ', '('), ' , ', ',')
FROM public.billableitemssql
GROUP BY REPLACE(REPLACE(REPLACE(filecontents4, ' )', ')'), '( ', '('), ' , ', ',')
ORDER BY 1 DESC;

/*
SELECT *, B.NumPerFC4 / CAST(A.Num AS FLOAT(1)) * 100 AS Pct
FROM (SELECT COUNT(1) AS Num FROM public.billableitemssql) AS A
CROSS JOIN (
	SELECT 
		COUNT(1) AS NumPerFC4
		, filecontents4
	FROM public.billableitemssql
	GROUP BY filecontents4
	) AS B
ORDER BY 4 DESC
*/