WITH 
	List0 AS (
		SELECT DISTINCT
			folderstructure
		FROM fileimport
	)
	, List1 AS (
		SELECT 
			folderstructure
			, strpos(REVERSE(folderstructure), '\') AS Split1
		FROM List0
	)
	, List2 AS (
		SELECT
			folderstructure
			, RIGHT(folderstructure, Split1 - 1) AS parentFolderName1
			, LEFT(folderstructure, LENGTH(folderstructure)-Split1) AS substr
		FROM List1		
	)
	, List3 AS (
		SELECT
			*
			, strpos(REVERSE(substr), '\') AS Split2
		FROM List2
	)
	, List4 AS (
		SELECT
			folderstructure
			, parentFolderName1
			, RIGHT(substr, Split2 - 1) AS parentFolderName2
			, LEFT(substr, LENGTH(substr)-Split2) AS substr
		FROM List3		
	)
	, List5 AS (
		SELECT
			*
			, strpos(REVERSE(substr), '\') AS Split3
		FROM List4
	)
	, List6 AS (
		SELECT 
			folderstructure
			, parentFolderName1
			, parentFolderName2
			, RIGHT(substr, Split3 - 1) AS parentFolderName3
		FROM List5
	)
	, List7 AS (
		SELECT 
			folderstructure
			, CASE WHEN parentFolderName3 = 'pricings' THEN null ELSE parentFolderName1 END AS parentFolderName1
			, CASE WHEN parentFolderName3 = 'pricings' THEN parentFolderName1 ELSE parentFolderName2 END AS parentFolderName2
			, CASE WHEN parentFolderName3 = 'pricings' THEN parentFolderName2 ELSE parentFolderName3 END AS parentFolderName3
		FROM List6
	)
UPDATE fileimport AS FI
SET parentFolderName1 = L.parentFolderName1
	, parentFolderName2 = L.parentFolderName2
	, parentFolderName3 = L.parentFolderName3
FROM List7 AS L
WHERE L.folderstructure = FI.folderstructure;

--SELECT * FROM fileimport;


TRUNCATE TABLE public.config;

INSERT INTO public.config (
	fileImportId
	, uid
	, label
	, feeDescription
	, reservationId
	, feeCategory
	, unitOfMeasure
	, shipperRate
	, warehouseRate
	)
SELECT 
	fileImportId
	, fileContents::json->>'uid' 						AS uid
	, fileContents::json->>'label' 						AS label
	, fileContents::json->>'feeDescription' 			AS feeDescription
	, (fileContents::json->>'reservationId')::integer 	AS reservationId
	, fileContents::json->>'feeCategory' 				AS feeCategory
	, fileContents::json->>'unitOfMeasure' 				AS unitOfMeasure
	, (fileContents::json->>'shipperRate')::numeric 	AS shipperRate
	, (fileContents::json->>'warehouseRate')::numeric 	AS warehouseRate
FROM public.fileimport
WHERE fileName = 'config.json';

--SELECT * FROM public.config;

TRUNCATE TABLE public.billableItemsSql;

INSERT INTO public.billableItemsSql (
	fileImportId
	, fileContents
	, fileContents2
	)
SELECT 
	fileImportId
	, fileContents
	, fileContents
FROM public.fileimport
WHERE fileName = 'billable_items.sql';

--SELECT * FROM public.billableItemsSql;
