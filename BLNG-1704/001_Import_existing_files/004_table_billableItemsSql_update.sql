UPDATE public.billableItemsSql
SET fileContents2 				= TRIM(both ' ' from fileContents)
	, fileContents3				= null
	, fileContents4				= null
	, fileContents5				= null
	, interval_startdate 		= null
	, interval_enddate 			= null
	, timezone 					= null
	, inboundtype 				= null
	, unitofmeasure 			= null
	, shipmenttype 				= null
	, inventoryGroupId 			= null
	, inventoryGroupName		= null
	, inventoryGroupIds 		= null
	, inventoryGroupNames		= null
	, itemId		 			= null
	, itemIds		 			= null
	, filecontents5subquery1	= null
	, filecontents5subquery2	= null
	, filecontents5_insert		= null
	, filecontents5_select		= null
	, filecontents5_from		= null
	, filecontents5_where		= null
	, filecontents5_groupby		= null
	, filecontents5_having		= null
	, filecontents5_orderby		= null
	, filecontents5_limit		= null
	, filecontents5_comment		= null;

DROP TYPE IF EXISTS arr;

CREATE TYPE arr AS (
	replaceString 	varchar
	, numLoops		int
	, numRows		int
	);

DO $body$
	
DECLARE arr1 arr[] := array[
	('	', 0, 1)
	, ('      ', 0, 1)
	, ('     ', 0, 1)
	, ('    ', 0, 1)
	, ('   ', 0, 1)
	, ('  ', 0, 1)
	];
v_replaceString	varchar;
v_numLoops		int;
v_numRows		int;

BEGIN

	FOREACH v_replaceString, v_numLoops, v_numRows IN ARRAY arr1 LOOP

		WHILE v_numRows > 0 LOOP
			v_numLoops = v_numLoops + 1;

			UPDATE public.billableItemsSql
			SET fileContents2 = REPLACE(fileContents2, v_replaceString, ' ')
			WHERE fileContents2 LIKE ('%' || v_replaceString || '%');

			GET DIAGNOSTICS v_numRows = ROW_COUNT;

			RAISE NOTICE 'Num Loops = %', v_numLoops;
		END LOOP;
		
	END LOOP;
	
END $body$;

DROP TYPE IF EXISTS arr;

--SELECT * FROM public.billableItemsSql;

UPDATE public.billableitemssql
SET filecontents3 = filecontents2;

CREATE TYPE arr AS (
	startSubstring 		varchar
	, endSubstring		varchar
	, fieldName			varchar
	, parameterName		varchar
	);

DO $body$

DECLARE arr1 arr[] := array[
	('(DATE({{start_date}}) + INTERVAL ' || CHR(39)	, CHR(39)	, 'interval_startdate'	, 'start_date_interval')
	, ('(DATE({{end_date}}) + INTERVAL ' || CHR(39)	, CHR(39)	, 'interval_enddate'	, 'end_date_interval')
	, ('::timestamptz AT TIME ZONE '	 || CHR(39)	, CHR(39)	, 'timezone'			, 'timezone')
	, ('item_id = '									, ' '		, 'itemid'				, 'item_id')
	, ('item_id IN ('								, ')'		, 'itemids'				, 'item_id_list')
	];
v_startString 	varchar;
v_endSubstring 	varchar;
v_fieldName		varchar;
v_parameterName	varchar;
v_sql			varchar;

-- delivery_type
	-- , 'Fulfillment::Models::RetailFulfillmentOrder' AS delivery_type
-- total_lpn_quantity
	-- SELECT 0 , 'NA' , 0 , CASE WHEN SUM(COALESCE(lpn_quantity, 0)) >= 10140 THEN 0 WHEN SUM(COALESCE(lpn_quantity, 0)) < 10140 THEN 10140 - SUM(COALESCE(lpn_quantity, 0)) END 
	-- SELECT 0 , 'NA' , 0 , CASE WHEN SUM(lpn_quantity) >= 6000 THEN 0 WHEN SUM(lpn_quantity) < 6000 THEN 6000 - SUM(lpn_quantity) END 
-- ?
	-- SELECT 2400 - max_quantity 
-- max_count_of_lpns
	-- SELECT CASE WHEN max_count_of_lpns < 1000 THEN 1000 - max_count_of_lpns ELSE 0 END 
-- ?
	-- , CASE WHEN quantity_received_carton_equivalents < 800 THEN 800 ELSE quantity_received_carton_equivalents END 
BEGIN

	FOREACH v_startString, v_endSubstring, v_fieldName, v_parameterName IN ARRAY arr1 LOOP

		v_sql = '
			UPDATE public.billableitemssql AS B
			SET ' || v_fieldName || ' 	= T.substringValue
				, fileContents3 	= T.newFileContents3
			FROM temp_replace AS T
			WHERE B.fileimportid = T.fileimportid;';

		DROP TABLE IF EXISTS temp_replace;

		CREATE TEMPORARY TABLE temp_replace (
			fileImportId 			integer 			NOT NULL
			, fileContents3			character varying	NOT NULL
			, stringStartChar		integer				NOT NULL
			, stringSubstring 		character varying	NULL
			, substringLength		integer				NULL
			, substringValue		character varying	NULL
			, newFileContents3		character varying	NULL
			);

		INSERT INTO temp_replace (
			fileImportId
			, fileContents3
			, stringStartChar
			)
		SELECT 
			fileimportid
			, filecontents3
			, strpos(filecontents3, v_startString) + LENGTH(v_startString)
		FROM public.billableitemssql
		WHERE filecontents3 LIKE '%' || v_startString || '%';

		UPDATE temp_replace
		SET stringSubstring = RIGHT(filecontents3, length(filecontents3) - stringStartChar);

		UPDATE temp_replace
		SET substringLength = STRPOS(stringSubstring, v_endSubstring);

		UPDATE temp_replace
		SET substringValue = SUBSTR(filecontents3, stringStartChar, substringLength);

		UPDATE temp_replace
		SET newFileContents3 = REPLACE(fileContents3, substringValue, '{{' || v_parameterName || '}}');	

		EXECUTE v_sql;

	END LOOP;
END $body$;

DROP TYPE arr;

UPDATE public.billableitemssql
SET interval_enddate = interval_startdate
WHERE interval_enddate = '{{start_date_interval}}';

UPDATE public.billableitemssql
SET interval_startdate = interval_enddate
WHERE interval_startdate = '{{end_date_interval}}';

--SELECT * FROM public.billableitemssql ORDER BY filecontents3;

WITH 
	List1 AS (
		SELECT 
			fileimportid
			, fileContents3
			, strpos(fileContents3, CHR(39) || ' AS delivery_type') - 1 AS stringEndChar
		FROM public.billableitemssql
		WHERE fileContents3 LIKE '%' || CHR(39) || ' AS delivery_type' || '%'
		)
	, List2 AS (
		SELECT 
			*
			, LEFT(fileContents3, stringEndChar) AS substr
		FROM List1
		)
	, List3 AS (
		SELECT 
			*
			, strpos(REVERSE(substr), CHR(39)) as stringStartCharRev
		FROM List2
		)
	, List4 AS (
		SELECT
			fileimportId
			, fileContents3
			, RIGHT(substr, stringStartCharRev-1) AS substringValue
		FROM List3
		)
	, List5 AS (
		SELECT
			fileimportId
			, substringValue
			, REPLACE(fileContents3, substringValue, '{{delivery_type}}') AS newFileContents
		FROM List4
		)
	UPDATE public.billableitemssql AS B
	SET deliverytype 	= T.substringValue
		, fileContents3 = T.newFileContents
	FROM List5 AS T
	WHERE B.fileimportid = T.fileimportid;

	UPDATE public.billableitemssql
	SET fileContents3 = REPLACE(REPLACE(fileContents3, CHR(39) || '{{', '{{'), '}}' || CHR(39), '}}');

--SELECT * FROM public.billableitemssql

UPDATE public.billableitemssql
SET filecontents3 = REPLACE(filecontents3, ('completed_at::timestamptz AT TIME ZONE ' || CHR(39) || timezone || CHR(39)), 'completed_at::timestamptz AT TIME ZONE {{timezone}}')
WHERE timezone IS NOT NULL;

--SELECT * FROM public.billableitemssql ORDER BY filecontents3;

UPDATE public.billableitemssql
SET filecontents4 = filecontents3;

CREATE TYPE arr AS (
	startSubstring 			varchar
	, endSubstring			varchar
	, endSubstring2			varchar
	, endSubstring3			varchar
	, fieldName				varchar
	, parameterName			varchar
	);

DO $body$

DECLARE arr1 arr[] := array[
	('AND inbound_type = '					|| CHR(39)	, CHR(39) || ' AND'	, CHR(39) || ' ', CHR(39)	, 'inboundtype'			, 'inbound_type')
	, ('AND unit_of_measure = '				|| CHR(39)	, CHR(39) || ' AND'	, CHR(39) || ' ', CHR(39)	, 'unitofmeasure'		, 'unit_of_measure2')
	, ('AND shipment_type = '				|| CHR(39)	, CHR(39) || ' AND'	, CHR(39) || ' ', CHR(39)	, 'shipmenttype'		, 'shipment_type')
	, ('AND inventory_group_name = '		|| CHR(39)	, CHR(39) || ' AND'	, CHR(39) || ' ', CHR(39)	, 'inventoryGroupName'	, 'inventory_group_name')
	, ('AND inventory_group_name IN ('		|| CHR(39)	, CHR(39) || ') AND', CHR(39) || ')', null		, 'inventoryGroupNames'	, 'inventory_group_name_list')
	, ('AND inventory_group_name NOT IN ('	|| CHR(39)	, CHR(39) || ') AND', CHR(39) || ')', null		, 'inventoryGroupNames'	, 'inventory_group_name_list')
	];
v_startString 			varchar;
v_endSubstring 			varchar;
v_endSubstring2 		varchar;
v_endSubstring3 		varchar;
v_fieldName				varchar;
v_parameterName			varchar;
v_sql					varchar;

BEGIN

	FOREACH v_startString, v_endSubstring, v_endSubstring2, v_endSubstring3, v_fieldName, v_parameterName IN ARRAY arr1 LOOP

		v_sql = '
			UPDATE public.billableitemssql AS B
			SET ' || v_fieldName || ' 	= T.substringValue
				, fileContents4 	= T.newFileContents4
			FROM temp_replace AS T
			WHERE B.fileimportid = T.fileimportid
				AND ' || v_fieldName || ' IS NULL;';

		DROP TABLE IF EXISTS temp_replace;

		CREATE TEMPORARY TABLE temp_replace (
			fileImportId 			integer 			NOT NULL
			, fileContents4			character varying	NOT NULL
			, stringStartWhere		integer				NULL
			, stringSubstring1 		character varying	NULL
			, stringStartChar		integer				NULL
			, stringSubstring2 		character varying	NULL
			, substringLength		integer				NULL
			, substringValue		character varying	NULL
			, newFileContents4		character varying	NULL
			);

		INSERT INTO temp_replace (
			fileImportId
			, fileContents4
			, stringStartWhere
			)
		SELECT 
			fileimportid
			, filecontents4
			, strpos(filecontents4, ' WHERE ')
		FROM public.billableitemssql
		WHERE filecontents4 LIKE '%' || v_startString || '%';

		UPDATE temp_replace
		SET stringSubstring1 = RIGHT(filecontents4, length(filecontents4) - stringStartWhere);
		
		UPDATE temp_replace
		SET stringStartChar = strpos(stringSubstring1, v_startString) + LENGTH(v_startString)
			- CASE
				WHEN RIGHT(v_startString, 2) = '(' || CHR(39)	THEN 2
				ELSE 1
			END;

		UPDATE temp_replace
		SET stringSubstring2 = RIGHT(stringSubstring1, length(stringSubstring1) - stringStartChar);

		UPDATE temp_replace
		SET substringLength = STRPOS(stringSubstring2, v_endSubstring)
			- CASE
				WHEN RIGHT(v_endSubstring, 2) = CHR(39) || ') ' THEN 0
				ELSE 1
			END;

		UPDATE temp_replace
		SET substringLength = STRPOS(stringSubstring2, v_endSubstring2)
			- CASE
				WHEN RIGHT(v_endSubstring2, 2) = CHR(39) || ') ' THEN 0
				ELSE 1
			END
		WHERE substringLength IS NULL
			OR substringLength <= 0;

		UPDATE temp_replace
		SET substringLength = STRPOS(stringSubstring2, v_endSubstring3)
			- CASE
				WHEN RIGHT(v_endSubstring3, 2) = CHR(39) || ') ' THEN 0
				ELSE 1
			END
		WHERE (substringLength IS NULL
			OR substringLength <= 0
			)
			AND v_endSubstring3 IS NOT NULL;

		UPDATE temp_replace
		SET substringValue = LEFT(stringSubstring2, substringLength);

		UPDATE temp_replace
		SET newFileContents4 = REPLACE(fileContents4, substringValue, CONCAT('{{', v_parameterName, '}}'));	
		
		EXECUTE v_sql;

	END LOOP;
END $body$;

DROP TYPE arr;

--SELECT * FROM temp_replace;

UPDATE public.billableitemssql
SET fileContents4 = REPLACE(REPLACE(fileContents4, CHR(39) || '{{', '{{'), '}}' || CHR(39), '}}');

SELECT * FROM public.billableitemssql ORDER BY filecontents4;