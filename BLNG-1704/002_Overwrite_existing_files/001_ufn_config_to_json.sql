CREATE OR REPLACE FUNCTION ufn_config_to_json (p_folderstructure character varying) RETURNS text

AS $$
WITH 
	Data1 AS (
		SELECT 
			C.uid
			, c.label
			, c.feeDescription
			, c.reservationId
			, c.feeCategory
			, c.unitOfMeasure
			, c.shipperRate
			, c.warehouseRate
			, bi.timeZone
			, bi.interval_startDate AS startDateInterval
			, bi.interval_endDate AS endDateInterval
			, bi.deliveryType
			, bi.inboundType
			, bi.shipmentType
			, bi.unitOfMeasure AS unitOfMeasure2
			, CASE WHEN bi.inventoryGroupName IS NOT NULL THEN bi.inventoryGroupName ELSE bi.inventoryGroupId END AS inventoryGroupName
		FROM public.fileimport AS FI1
		INNER JOIN public.config AS C
			ON FI1.fileimportid = C.fileimportid
		INNER JOIN public.fileimport AS FI2
			ON FI1.folderstructure = FI2.folderstructure
				AND C.fileimportid <> FI2.fileimportid
		INNER JOIN public.billableitemssql AS BI
			ON FI2.fileimportid = BI.fileimportid
		WHERE FI1.folderstructure = p_folderstructure
		LIMIT 1
		)
SELECT
	CONCAT(
'{
    "uid": '			, '"'	, uid, '",
    "label": '			, '"'	, label, '",
    "feeDescription": '	, '"'	, feeDescription, '",
    "reservationId": '			, reservationId, ',
    "feeCategory": '	, '"'	, feeCategory, '",
    "unitOfMeasure": '	, '"'	, unitOfMeasure, '",
    "shipperRate": '			, shipperRate, ',
    "warehouseRate": '			, warehouseRate, ',
	"timezone": '				, CASE WHEN timezone IS NULL 			THEN 'null' ELSE CONCAT('"', timezone			, '"') END, ',
    "startDateInterval": '		, CASE WHEN startDateInterval IS NULL 	THEN 'null' ELSE CONCAT('"', startDateInterval	, '"') END, ',
    "endDateInterval": '		, CASE WHEN endDateInterval IS NULL 	THEN 'null' ELSE CONCAT('"', endDateInterval	, '"') END, ',
    "deliveryType": '			, CASE WHEN deliveryType IS NULL 		THEN 'null' ELSE CONCAT('"', deliveryType		, '"') END, ',
    "inboundType": '			, CASE WHEN inboundType IS NULL 		THEN 'null' ELSE CONCAT('"', inboundType		, '"') END, ',
    "shipmentType": '			, CASE WHEN shipmenttype IS NULL 		THEN 'null' ELSE CONCAT('"', shipmenttype		, '"') END, ',
    "unitOfMeasure2": '			, CASE WHEN unitOfMeasure2 IS NULL 		THEN 'null' ELSE CONCAT('"', unitOfMeasure2		, '"') END, ',
    "inventoryGroupName": '		, CASE WHEN inventoryGroupName IS NULL 	THEN 'null' ELSE CONCAT('"', inventoryGroupName	, '"') END, '
}') AS json_output
FROM Data1;

$$
LANGUAGE SQL;

--SELECT * FROM ufn_config_to_json ('C:\Git\biz-analytics-pat\environments\production\pricings\frito-r1035\inbound\per-lpn-received-palletized');