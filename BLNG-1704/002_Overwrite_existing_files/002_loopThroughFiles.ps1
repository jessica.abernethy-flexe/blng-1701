$MyServer = "localhost";
$MyPort = 5432;
$MyDB = "BizAnalyticsPAT_Import";
$MyUid ="postgres"
$MyPass ="postgres"

$targDir = "C:\Git\biz-analytics-pat\environments\production\pricings\";

$childFiles = $(Get-ChildItem -Path $targDir -Recurse -File );

foreach($file in $childFiles) 
{
    #$fileName = $file.Name  #file name
    $filePath = $file.FullName  #entire file path
    $parentPath = ($file.Directory).FullName    #parent folder path
    $fileContents = Get-Content -Path $filePath

    $filePath  # print it so we know where we are

    if ($fileContents -ne $null)
    {
        $parentPath = $parentPath.Replace("'", "''");
        #$fileName = $fileName.Replace("'", "''");
        $fileContents = $fileContents.Replace("'", "''");
        $newFileContents = "";

		$query = "SELECT * FROM ufn_config_to_json ('$parentPath');"
		$conn = New-Object System.Data.Odbc.OdbcConnection
        $conn.ConnectionString= "Driver={PostgreSQL UNICODE(x64)};Server=$MyServer;Port=$MyPort;Database=$MyDB;Uid=$MyUid;Pwd=$MyPass;"
    
        $cmd = new-object System.Data.Odbc.OdbcCommand($query,$conn)

        $conn.open()
        $cmd.ExecuteNonQuery()
			
		$ds = New-Object system.Data.DataSet
		(New-Object System.Data.Odbc.OdbcDataAdapter($cmd)).Fill($ds) | Out-Null
        $newFileContents = $ds.Tables[0] | Out-String;

        Set-Content -Path $filePath -Value $newFileContents

		#Set-Content -Path $filePath -Value $ds.Tables[0]		
		
		#$cmd.ExecuteReader() | Out-File -FilePath $filePath

        $conn.close()
		
		break
    }
}