DROP TABLE IF EXISTS temp_config;

CREATE TEMPORARY TABLE temp_config (
    uid 				character varying 	NOT NULL	COLLATE pg_catalog."default",
    label 				character varying 	NULL		COLLATE pg_catalog."default",
    feeDescription 		character varying 	NULL		COLLATE pg_catalog."default",
    reservationId 		integer				NULL,
    feeCategory 		character varying 	NULL		COLLATE pg_catalog."default",
    unitOfMeasure 		character varying 	NULL		COLLATE pg_catalog."default",
    shipperRate 		numeric				NULL,
    warehouseRate 		numeric				NULL,
    timeZone 			character varying	NULL 		COLLATE pg_catalog."default", 
    startDateInterval 	character varying	NULL 		COLLATE pg_catalog."default", 
    endDateInterval 	character varying	NULL 		COLLATE pg_catalog."default", 
    deliveryType 		character varying	NULL 		COLLATE pg_catalog."default", 
    inboundType 		character varying	NULL 		COLLATE pg_catalog."default", 
    shipmentType 		character varying	NULL 		COLLATE pg_catalog."default", 
    unitOfMeasure2 		character varying	NULL 		COLLATE pg_catalog."default", 
    inventoryGroupName	character varying	NULL 		COLLATE pg_catalog."default",
	json1				json				NULL,
	jsonb1				jsonb				NULL,
	jsonb2				text				NULL
	);

INSERT INTO temp_config (
	uid
	, label
	, feeDescription
	, reservationId
	, feeCategory
	, unitOfMeasure
	, shipperRate
	, warehouseRate
	, timeZone
	, startDateInterval
	, endDateInterval
	, deliveryType
	, inboundType
	, shipmentType
	, unitOfMeasure2
	, inventoryGroupName	
	)
SELECT 
	C.uid
	, c.label
	, c.feeDescription
	, c.reservationId
	, c.feeCategory
	, c.unitOfMeasure
	, c.shipperRate
	, c.warehouseRate
	, bi.timeZone
	, bi.interval_startDate AS startDateInterval
	, bi.interval_endDate AS endDateInterval
	, bi.deliveryType
	, bi.inboundType
	, bi.shipmentType
	, bi.unitOfMeasure AS unitOfMeasure2
	, CASE WHEN bi.inventoryGroupName IS NOT NULL THEN bi.inventoryGroupName ELSE bi.inventoryGroupId END AS inventoryGroupName
FROM public.config AS C
INNER JOIN public.fileimport AS FI1
	ON C.fileimportid = FI1.fileimportid
INNER JOIN public.fileimport AS FI2
	ON FI1.folderstructure = FI2.folderstructure
		AND C.fileimportid <> FI2.fileimportid
INNER JOIN public.billableitemssql AS BI
	ON FI2.fileimportid = BI.fileimportid
LIMIT 1;

UPDATE temp_config
SET json1 = (SELECT row_to_json(temp_config) AS Json1 FROM temp_config);

UPDATE temp_config
SET jsonb1 = json1::jsonb;

UPDATE temp_config
SET jsonb2 = jsonb_pretty(jsonb1);

SELECT *
FROM temp_config
