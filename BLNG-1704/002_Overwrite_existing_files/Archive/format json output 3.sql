WITH 
	Data1 AS (
		SELECT 
			C.uid
			, c.label
			, c.feeDescription
			, c.reservationId
			, c.feeCategory
			, c.unitOfMeasure
			, c.shipperRate
			, c.warehouseRate
			, bi.timeZone
			, bi.interval_startDate AS startDateInterval
			, bi.interval_endDate AS endDateInterval
			, bi.deliveryType
			, bi.inboundType
			, bi.shipmentType
			, bi.unitOfMeasure AS unitOfMeasure2
			, CASE WHEN bi.inventoryGroupName IS NOT NULL THEN bi.inventoryGroupName ELSE bi.inventoryGroupId END AS inventoryGroupName
		FROM public.config AS C
		INNER JOIN public.fileimport AS FI1
			ON C.fileimportid = FI1.fileimportid
		INNER JOIN public.fileimport AS FI2
			ON FI1.folderstructure = FI2.folderstructure
				AND C.fileimportid <> FI2.fileimportid
		INNER JOIN public.billableitemssql AS BI
			ON FI2.fileimportid = BI.fileimportid
		LIMIT 1
		)
SELECT
	*
	, jsonb_pretty(row_to_json(Data1)::jsonb) AS Jsonb1
	, CONCAT(
'{
    "uid": '			, '"'	, uid, '",
    "label": '			, '"'	, label, '",
    "feeDescription": '	, '"'	, feeDescription, '",
    "reservationId": '			, reservationId, ',
    "feeCategory": '	, '"'	, feeCategory, '",
    "unitOfMeasure": '	, '"'	, unitOfMeasure, '",
    "shipperRate": '			, shipperRate, ',
    "warehouseRate": '			, warehouseRate, ',
	"timezone": '				, CASE WHEN timezone IS NULL 			THEN 'null' ELSE CONCAT('"', timezone			, '"') END, ',
    "startDateInterval": '		, CASE WHEN startDateInterval IS NULL 	THEN 'null' ELSE CONCAT('"', startDateInterval	, '"') END, ',
    "endDateInterval": '		, CASE WHEN endDateInterval IS NULL 	THEN 'null' ELSE CONCAT('"', endDateInterval	, '"') END, ',
    "deliveryType": '			, CASE WHEN deliveryType IS NULL 		THEN 'null' ELSE CONCAT('"', deliveryType		, '"') END, ',
    "inboundType": '			, CASE WHEN inboundType IS NULL 		THEN 'null' ELSE CONCAT('"', inboundType		, '"') END, ',
    "shipmentType": '			, CASE WHEN shipmenttype IS NULL 		THEN 'null' ELSE CONCAT('"', shipmenttype		, '"') END, ',
    "unitOfMeasure2": '			, CASE WHEN unitOfMeasure2 IS NULL 		THEN 'null' ELSE CONCAT('"', unitOfMeasure2		, '"') END, ',
    "inventoryGroupName": '		, CASE WHEN inventoryGroupName IS NULL 	THEN 'null' ELSE CONCAT('"', inventoryGroupName	, '"') END, '
}') AS Jsonb2
FROM Data1;

