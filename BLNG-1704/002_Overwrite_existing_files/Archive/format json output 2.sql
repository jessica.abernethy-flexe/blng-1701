WITH 
	Data1 AS (
		SELECT 
			C.uid
			, c.label
			, c.feeDescription
			, c.reservationId
			, c.feeCategory
			, c.unitOfMeasure
			, c.shipperRate
			, c.warehouseRate
			, bi.timeZone
			, bi.interval_startDate AS startDateInterval
			, bi.interval_endDate AS endDateInterval
			, bi.deliveryType
			, bi.inboundType
			, bi.shipmentType
			, bi.unitOfMeasure AS unitOfMeasure2
			, CASE WHEN bi.inventoryGroupName IS NOT NULL THEN bi.inventoryGroupName ELSE bi.inventoryGroupId END AS inventoryGroupName
		FROM public.config AS C
		INNER JOIN public.fileimport AS FI1
			ON C.fileimportid = FI1.fileimportid
		INNER JOIN public.fileimport AS FI2
			ON FI1.folderstructure = FI2.folderstructure
				AND C.fileimportid <> FI2.fileimportid
		INNER JOIN public.billableitemssql AS BI
			ON FI2.fileimportid = BI.fileimportid
		LIMIT 1
		)
	, Data2 AS (
		SELECT
			*
			, row_to_json(Data1) AS Json1
		FROM Data1
		)
	, Data3 AS (
		SELECT
			*
			, json1::jsonb AS Jsonb1a
			, jsonb_build_object(
				'uid', uid,
				'label', label,
				'feeDescription', feeDescription,
				'reservationId', reservationId,
				'feeCategory', feeCategory,
				'unitOfMeasure', unitOfMeasure,
				'shipperRate', shipperRate,
				'warehouseRate', warehouseRate,
				'timeZone', timeZone,
				'startDateInterval', startDateInterval,
				'endDateInterval', endDateInterval,
				'deliveryType', deliveryType,
				'inboundType', inboundType,
				'shipmentType', shipmentType,
				'unitOfMeasure2', unitOfMeasure2,
				'inventoryGroupName', inventoryGroupName) AS Jsonb2a
		FROM Data2
		)
SELECT
	*
	, jsonb_pretty(jsonb1a) AS Jsonb1b
	, jsonb_pretty(jsonb2a) AS Jsonb2b
FROM Data3
