-- daily_lpn_count_billable_items
FROM 
	billdata.inventory_snapshots
WHERE 
	snapshot_at > current_date - interval '3' month

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- daily_lpn_count_by_sku_billable_items_no_ib_check
FROM billdata.inventory_snapshots AS ins
LEFT JOIN billdata.inventory_tracking_data AS itd
	ON itd.id = ins.inventory_tracking_data_id
LEFT JOIN billdata.item_properties AS ip
	ON ip.item_id = ins.item_id
		AND ip.unit_of_measure = ins.unit_of_measure
WHERE
	ins.snapshot_at > current_date - interval '3' month
	AND ins.quantity_stored > 0

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- shipped_lpn_billable_items
FROM billdata.shipments AS s
INNER JOIN billdata.shipment_items AS si
	ON si.shipment_id = s.id 
INNER JOIN billdata.shipment_item_to_inventory_tracking_data AS sitd
	ON sitd.shipment_item_id = si.id 
WHERE 
	s.completed_at > current_date - interval '3' month
	AND si.quantity <> 0
	AND sitd.inventory_tracking_data_id IS NOT NULL 
	
-- shipped_lpn_detail_billable_items
FROM billdata.shipments AS s
INNER JOIN billdata.shipment_items AS si
	ON si.shipment_id = s.id 
INNER JOIN billdata.shipment_item_to_inventory_tracking_data AS sitd
	ON sitd.shipment_item_id = si.id
LEFT JOIN billdata.inventory_tracking_data AS itd
	ON itd.id = sitd.inventory_tracking_data_id
LEFT JOIN billdata.item_properties AS ip
	ON ip.item_id = si.item_id
	AND ip.unit_of_measure = si.unit_of_measure
WHERE 
	s.completed_at > current_date - interval '3' month
	AND si.quantity <> 0
	AND sitd.inventory_tracking_data_id IS NOT NULL

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- daily_stored_cubic_feet
FROM billdata.inventory_snapshots AS ins
LEFT JOIN billdata.item_properties AS cip
	ON cip.item_id = ins.item_id
		AND cip.unit_of_measure = 'carton'
LEFT JOIN billdata.item_properties AS eip
	ON eip.item_id = ins.item_id
		AND eip.unit_of_measure = 'each'
WHERE 
	ins.snapshot_at > current_date - interval '3' month
	AND ins.quantity_stored > 0 

-- daily_stored_quantity
FROM billdata.inventory_snapshots AS ins
LEFT JOIN billdata.inventory_tracking_data AS itd
	ON itd.id = ins.inventory_tracking_data_id
LEFT JOIN billdata.item_properties AS eip
	ON eip.item_id = ins.item_id
		AND eip.unit_of_measure = 'each'
LEFT JOIN billdata.item_properties AS cip
	ON cip.item_id = ins.item_id
		AND cip.unit_of_measure = 'carton'
WHERE
	ins.snapshot_at > current_date - interval '3' month
	AND ins.quantity_stored > 0

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- outbound_pallet_equivalents_with_carton_remainder
FROM billdata.shipments AS s
INNER JOIN billdata.shipment_items AS si 
	ON si.shipment_id = s.id 
LEFT JOIN billdata.item_properties AS eip 
	ON eip.item_id = si.item_id
		AND eip.unit_of_measure = 'each'
LEFT JOIN billdata.item_properties AS cip 
	ON cip.item_id = si.item_id
		AND cip.unit_of_measure = 'carton'
WHERE 
	s.completed_at > current_date - interval '3' month
	AND si.quantity <> 0

-- outbound_whole_carton_each_remainder
FROM billdata.shipments AS s
INNER JOIN billdata.shipment_items AS si
	ON si.shipment_id = s.id
LEFT JOIN billdata.item_properties AS eip
	ON eip.item_id = si.item_id
		AND eip.unit_of_measure = 'each'
LEFT JOIN billdata.item_properties AS cip
	ON cip.item_id = si.item_id
		AND cip.unit_of_measure = 'carton'
WHERE 
	s.completed_at > current_date - interval '3' month
	AND si.quantity > 0

---------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
-- shipped_items_by_inventory_group
FROM billdata.shipments AS s
LEFT JOIN billdata.shipment_items AS si
	ON si.shipment_id = s.id 
LEFT JOIN billdata.item_properties AS eip
	ON eip.item_id = si.item_id
		AND eip.unit_of_measure = 'each'
LEFT JOIN billdata.item_properties AS cip
	ON cip.item_id = si.item_id
		AND cip.unit_of_measure = 'carton'
WHERE
	s.completed_at > current_date - interval '3' month
	
-- shipped_items
FROM billdata.shipments AS s
INNER JOIN billdata.shipment_items AS si
	ON si.shipment_id = s.id 
LEFT JOIN billdata.item_properties AS eip
	ON eip.item_id = si.item_id
		AND eip.unit_of_measure = 'each'
LEFT JOIN billdata.item_properties AS cip
	ON cip.item_id = si.item_id
		AND cip.unit_of_measure = 'carton'
WHERE
	s.completed_at > current_date - interval '3' month

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- per_item_unload_billable_items
FROM billdata.inbounds AS i
INNER JOIN billdata.inbound_items AS ins
	ON ins.inbound_id = i.id
LEFT JOIN billdata.item_properties AS eip
	ON eip.item_id = ins.item_id
		AND eip.unit_of_measure = 'each'
LEFT JOIN billdata.item_properties AS cip
	ON cip.item_id = ins.item_id
		AND cip.unit_of_measure = 'carton'
WHERE 
	i.completed_at > current_date - interval '3' month
	AND ins.quantity_received > 0 
	
-- whole_carton_each_remainder_inbound
FROM billdata.inbounds AS i
INNER JOIN billdata.inbound_items AS ins
	ON ins.inbound_id = i.id
LEFT JOIN billdata.item_properties AS eip
	ON eip.item_id = ins.item_id
		AND eip.unit_of_measure = 'each'
LEFT JOIN billdata.item_properties AS cip
	ON cip.item_id = ins.item_id
		AND cip.unit_of_measure = 'carton'
WHERE 
	i.completed_at > current_date - interval '3' month
	AND ins.quantity_received > 0

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- per_lpn_received_detail_billable_items
FROM billdata.inbounds AS i
INNER JOIN billdata.inbound_items AS ins
	ON ins.inbound_id = i.id
LEFT JOIN billdata.item_properties AS eip
	ON eip.item_id = ins.item_id
		AND eip.unit_of_measure = 'each'
LEFT JOIN billdata.item_properties AS cip
	ON cip.item_id = ins.item_id
		AND cip.unit_of_measure = 'carton'
LEFT JOIN billdata.inventory_tracking_data AS itd 
	ON itd.id = ins.inventory_tracking_data_id
WHERE 
	i.completed_at > current_date - interval '3' month
	AND ins.quantity_received > 0 
	AND ins.inventory_tracking_data_id IS NOT NULL


---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- daily_stored_lpns_including_crossdock_by_inventory_group_no_ib_check
FROM billdata.shipments AS s
INNER JOIN billdata.shipment_items AS si
	ON s.id = si.shipment_id 
INNER JOIN billdata.shipment_item_to_inventory_tracking_data AS sitd
	ON sitd.shipment_item_id = si.id
LEFT JOIN (SELECT inventory_tracking_data_id FROM billdata.inventory_snapshots) AS itdx	-- TODO: why are we selecting a single column and not aggregating?  why the subquery?
	ON itdx.inventory_tracking_data_id = sitd.inventory_tracking_data_id
WHERE 
	s.completed_at > current_date - interval '3' month
	AND si.quantity > 0
	AND itdx.inventory_tracking_data_id IS NULL 
	AND sitd.inventory_tracking_data_id IS NOT NULL

-- daily_stored_lpns_including_crossdock_by_inventory_group_no_ib_check
FROM billdata.shipments AS s
INNER JOIN billdata.shipment_items AS si 
	ON s.id = si.shipment_id 
INNER JOIN billdata.shipment_item_to_inventory_tracking_data AS sitd
	ON sitd.shipment_item_id = si.id
LEFT JOIN (SELECT inventory_tracking_data_id FROM billdata.inventory_snapshots) AS itdx	-- TODO: why are we selecting a single column and not aggregating?  why the subquery?
	ON itdx.inventory_tracking_data_id = sitd.inventory_tracking_data_id
WHERE 
	s.completed_at > current_date - interval '3' month
	AND si.quantity > 0
	AND itdx.inventory_tracking_data_id IS NULL 
	AND sitd.inventory_tracking_data_id IS NOT NULL

-- daily_stored_lpns_including_crossdock_by_inventory_group_no_ib_check
FROM billdata.shipments AS s
INNER JOIN billdata.shipment_items AS si
	ON s.id = si.shipment_id 
INNER JOIN billdata.shipment_item_to_inventory_tracking_data AS sitd
	ON sitd.shipment_item_id = si.id
LEFT JOIN (SELECT inventory_tracking_data_id FROM billdata.inventory_snapshots) AS itdx	-- TODO: why are we selecting a single column and not aggregating?  why the subquery?
	ON itdx.inventory_tracking_data_id = sitd.inventory_tracking_data_id
WHERE 
	s.completed_at > current_date - interval '3' month
	AND si.quantity > 0
	AND itdx.inventory_tracking_data_id IS NULL 
	AND sitd.inventory_tracking_data_id IS NOT NULL

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- per_lpn_received_billable_items
FROM billdata.inbounds AS i
INNER JOIN billdata.inbound_items AS ins
	ON ins.inbound_id = i.id
WHERE 
	i.completed_at > current_date - interval '3' month
	AND ins.quantity_received > 0 
